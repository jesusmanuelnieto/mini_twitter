// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require jquery_ujs
//= require jquery-ui/widgets/dialog
//= require_tree .

function show_user(id) {
	$.getJSON( "users/" + id, function( data ) {
		$("#name").text(data.name);
		$("#email").text(data.email);
		$( "#show" ).dialog({closeText: ""});
	});
}

function edit_user(id) {
	$.getJSON( "users/" + id, function( data ) {
		$("#editId").val(data.id);
		$("#editName").val(data.name);
		$("#editEmail").val(data.email);
		$( "#edit" ).dialog({closeText: "Cerrar Edición Sin guardar"});
	});
}

function edituserAjax (){
	//Datos
	var user = {
		'user[name]'	:  $("#editName").val(),
		'user[email]'	:  $("#editEmail").val(),
	}

	//Enviamos la solicitud
	$.ajax({ url: 'users/'+parseInt($("#editId").val()),
		type: 'PUT',
		beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
		data: user,
		success: function(response) {
			//Si todo fue correcto:
			$("#editId").val(0);
			$("#editName").val("");
			$("#editEmail").val("");;
		}
	});
}

function micropostlist(id) {
	$.getJSON( "users/micropostlist/"+id, function( data ) {
		//Vaciamos los campos variables de la capa de post.
		$("#micropostUl").empty();
		$("#micropostName").text("");

		//Introducimos el nombre de usario
		$("#micropostName").text(data.name);
			
		//Ahora escribiremos los micropost
		data.microposts.forEach(function (micropost,index){
			//Nos aseguramos que siempre entra el dato como cadena de texto.
			$("#micropostUl").append('<li>'+micropost.content.toString()+'</li>');
		});
		
		
		//Mostramos la capa
		$( "#micropostList" ).dialog({closeText: "Cerrar Dialog"});
	});
}


