Rails.application.routes.draw do
  #recordar que el orden es importante, evalúa primero las primeras líneas, si no encuentra patrón, seguirá hacia abajo
  resources :microposts
  get "users/micropostlist/:id",  to: "users#micropostlist"
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
